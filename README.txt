MeCab Search provides MeCab functionality to Search module.


Requirements
------------

MeCab Search requires that your server be able to use MeCab functionality
through php-mecab library.

* MeCab is a Japanese morphological analyzer created by
  [Taku KUDO][mecab_creator]. For more detail, see [MeCab official page][mecab].
* php-mecab is MeCab php binding created by
  [Ryusuke SEKIYAMA][php_mecab_creator]. For more detail,
  see [php-mecab GitHub page][php_mecab].

Note:

* MeCab and php-mecab are not included in this module.
* You need to install MeCab and php-mecab separately.


Installation
------------

MeCab Search can be installed via the standard Drupal installation process.

Before installing MeCab Search, please make sure php-mecab is already available
on your server. Concretely, a function `mecab_split` needs to be available in
your Drupal code to make the module work.

After preparing php-mecab and installing the module, you need to set proper
settings for MeCab Search on the admin page.

http:://your_drupal_root/admin/config/search/mecab.

On the above page, follow the following steps to make MeCab Search work well.

1. Enable MeCab Search by checking on the check box.
2. Enable noun filter to limit the types of words to noun if you want.


API
---

MeCab Search doesn't have open APIs for other modules.


Credits
-------

Development of MeCab Search is sponsored by Studio Umi+ (http://studio-umi.jp).

A special thanks goes out to Taku KUDO (http://chasen.org/~taku/) who created
MeCab and Ryusuke SEKIYAMA (http://blog.r-sky.jp/) who created php-mecab.


[mecab_creator]: http://chasen.org/~taku/
[mecab]: http://mecab.googlecode.com/svn/trunk/mecab/doc/
[php_mecab_creator]: http://blog.r-sky.jp/
[php_mecab]: https://github.com/rsky/php-mecab
