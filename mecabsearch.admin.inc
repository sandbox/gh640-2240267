<?php

/**
 * @file
 * Admin page functions for MeCab search.
 */

/**
 * Implements hook_form().
 *
 * This function draws MeCab admin page form.
 *
 * @see mecabsearch_menu()
 * @see mecabsearch_admin_settings_form()
 */
function mecabsearch_admin_settings_form($form) {

  // Get the related MeCab Search variable.
  $mecabsearch_enabled = variable_get('mecabsearch_enabled', 0);

  // Add checkbox to switch MeCab Search on/off.
  $form['mecabsearch_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable MeCab search'),
    '#default_value' => $mecabsearch_enabled,
    '#description' => t('Check this if you want to enable MeCab Search functionality.')
    . ' ' . t('Before check this on, please make sure MeCab is available on your PHP environment.')
    . ' ' . t('It is highly recommended to disable simple CJK handling and change Minimum word indexing length to 2 when this checkbox is checked.')
    . ' ' . t('To set them, visit <a href="@link">@anchor_text</a>.',
          array(
            '@link' => 'settings',
            '@anchor_text' => 'Search Settings page',
          )),
  );

  // Add heckbox to switch noun filter MeCab Search on/off.
  $form['mecabsearch_noun_filter_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable MeCab search noun filter'),
    '#default_value' => variable_get('mecabsearch_noun_filter_enabled', 0),
    '#description' => t('Check this if you want to enable noun filter for MeCab search.'),
    '#disabled' => !$mecabsearch_enabled,
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'mecabsearch_admin_settings_form_submit';

  return $form;
}
